/*
 * Copyright (c) 2020 杨军
 * easyui-demo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2. 
 * You may obtain a copy of Mulan PSL v2 at:
 *         http://license.coscl.org.cn/MulanPSL2 
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v2 for more details.
 */
package yj95.top.pagination.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.util.StrUtil;
import cn.hutool.db.Entity;

@RestController
@RequestMapping("/pagination")
public class PaginationController {
    
    @GetMapping("/showContent")
    public List<Entity> showContent(int page, int pageSize) {
        page = page <= 0 ? 1 : page;
        pageSize = pageSize <= 0 ? 10 : pageSize;
        int idx = 1;
        List<Entity> list = new ArrayList<>();
        for (int index = 1; index <= pageSize; ++index) {
            idx = (page - 1) * pageSize + index;
            list.add(Entity.create().set("id", idx).set("user", StrUtil.format("User{}", idx)));
        }
        return list;
    }
    
}
