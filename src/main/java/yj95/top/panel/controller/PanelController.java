package yj95.top.panel.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/panel")
public class PanelController {
    
    @GetMapping("/content")
    public String content() {
        return "这是面板的内容";
    }
    
    @GetMapping("/newContent")
    public String newContent() {
        return "<p>这是一段新的内容</p>";
    }
    
}
