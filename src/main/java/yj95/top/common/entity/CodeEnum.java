/*
 * Copyright (c) 2020 杨军
 * easyui-demo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2. 
 * You may obtain a copy of Mulan PSL v2 at:
 *         http://license.coscl.org.cn/MulanPSL2 
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v2 for more details.
 */
package yj95.top.common.entity;

/**
 * 处理结果代码枚举
 * @author 杨军
 * @date 2020-07-19
 */
public enum CodeEnum {
    /** 成功 */
    SUCCESS(0),
    /** 失败 */
    FAILURE(-1),
    /** 异常 */
    EXCEPTION(-2);
    
    private final int value;

    private CodeEnum(int value) {
        this.value = value;
    }
    
    public int value() {
        return value;
    }
    
}
