/*
 * Copyright (c) 2020 杨军
 * easyui-demo is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2. 
 * You may obtain a copy of Mulan PSL v2 at:
 *         http://license.coscl.org.cn/MulanPSL2 
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v2 for more details.
 */
package yj95.top.common.entity;

/**
 * JSON返回结果
 * @param <T> 数据类型
 * @author 杨军
 * @date 2020-07-19
 */
public class JsonResult<T> {
    /** 处理结果，参考{@link CodeEnum} */
    private int code;
    /** 处理描述 */
    private String msg;
    /** 返回数据 */
    private T data;
    
    public JsonResult(int code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public JsonResult(int code, String msg, T data) {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    
    public static <T> JsonResult<T> ok(String msg) {
        return ok(msg, null);
    }
    
    public static <T> JsonResult<T> ok(String msg, T data) {
        return new JsonResult<T>(CodeEnum.SUCCESS.value(), msg, data);
    }
    
    public static <T> JsonResult<T> error(String msg) {
        return error(msg, null);
    }
    
    public static <T> JsonResult<T> error(String msg, T data) {
        return new JsonResult<T>(CodeEnum.FAILURE.value(), msg, data);
    }
    
    public static <T> JsonResult<T> exception(String msg) {
        return exception(msg, null);
    }
    
    public static <T> JsonResult<T> exception(String msg, T data) {
        return new JsonResult<T>(CodeEnum.EXCEPTION.value(), msg, data);
    }
    
    public JsonResult<T> code(int code) {
        this.code = code;
        return this;
    }
    
    public int code() {
        return code;
    }
    
    public JsonResult<T> msg(String msg) {
        this.msg = msg;
        return this;
    }
    
    public String msg() {
        return msg;
    }
    
    public JsonResult<T> data(T data) {
        this.data = data;
        return this;
    }
    
    public T data() {
        return data;
    }
    
}
